import {
  BrowserRouter as Router,
  Link,
  Route,
  Routes,
} from 'react-router-dom'
import React from 'react'
import { Nav, Navbar, NavItem } from 'reactstrap'
import Login from './pages/login'
import Chat from './pages/chat'
import Users from './pages/users'
import { PrivateRoute } from './helpers/private-router.helper'
import { UserRole } from './common/enums/app/user-role.enum'
import EditUsers from './pages/edit-users'
import EditUser from './pages/edit-user'

/* eslint arrow-body-style: off */
const App: React.FC = () => {
  // todo
  return (
    <>
      <Router>
        <Navbar color="light" light expand="md">
          <Nav navbar>
            <NavItem className="pr-4">
              <Link to="/">Chat</Link>
            </NavItem>
            <NavItem className="pr-4">
              <Link to="/users">Users</Link>
            </NavItem>
            <NavItem className="pr-4">
              <Link to="/login">Log out</Link>
            </NavItem>
          </Nav>
        </Navbar>

        <Routes>
          <Route path="/login" element={<Login />} />
          <Route
            path="/"
            element={
              <PrivateRoute roles={[UserRole.USER, UserRole.ADMIN]} component={Chat} />
            }
          />
          <Route
            path="/users"
            element={
              <PrivateRoute roles={[UserRole.ADMIN]} component={Users} />
            }
          />
          <Route
            path="/users/edit"
            element={
              <PrivateRoute roles={[UserRole.ADMIN]} component={EditUsers} />
            }
          />
          <Route
            path="/users/edit/:id"
            element={
              <PrivateRoute roles={[UserRole.ADMIN]} component={EditUser} />
            }
          />
        </Routes>
      </Router>
    </>
  )
}

export default App
