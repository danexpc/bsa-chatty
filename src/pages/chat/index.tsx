import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import Header from '../../components/header'
import MessageList from '../../components/messageList'
import MessageInput from '../../components/messageInput'
import { Preloader } from '../../components/preloader'

import './chat.css'
import { countUniqueParticipants, getLastMessageDate } from './utils'
import { prepareMessagesForRender } from './helpers'
import { chatActionCreator } from '../../store/actions'
import { RootState } from '../../store/store'
import { LoadingStatus } from '../../common/enums/app/loading-status.enum'
import MessageTransformer from '../../utils/MessageTransformer'
import { getFormattedDateForLastMessage } from '../../utils/DateFormatter'

const Chat: React.FC = () => {
  const dispatch = useDispatch()

  const { loading } = useSelector((state: RootState) => state.chat)
  const { user } = useSelector((state: RootState) => state.auth)
  const messages = useSelector((state: RootState) => state.chat.messages.map(
    MessageTransformer.transform,
  ))

  useEffect(() => {
    dispatch(chatActionCreator.getAllMessages())
  }, [])

  if (loading === LoadingStatus.LOADING) {
    return <Preloader />
  }

  return (
    <div className="chat container">
      <Header
        chatName="My Chat"
        participantsCount={countUniqueParticipants(messages)}
        messagesCount={messages.length}
        lastMessageDate={getFormattedDateForLastMessage(
          getLastMessageDate(messages),
        )}
      />
      <MessageList>
        {prepareMessagesForRender(
          messages,
          user,
        )}
      </MessageList>
      <MessageInput />
    </div>
  )
}

export default Chat
