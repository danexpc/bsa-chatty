import { IMessage } from '../../interfaces/common/message'

export const countUniqueParticipants = (messages: IMessage[]): number => {
  const participants: number[] = []

  messages.forEach((message) => {
    if (!participants.includes(message.userId)) {
      participants.push(message.userId)
    }
  })

  return participants.length
}

export const getLastMessageDate = (
  messages: IMessage[],
): Date => {
  if (messages.length === 0) {
    return new Date()
  }
  return messages[messages.length - 1].createdAt
}
