import React from 'react'
import { v4 as uuidv4 } from 'uuid'
import { IMessage } from '../../interfaces/common/message'
import {
  getFormattedDateForDivider,
  getFormattedTimeForMessage,
} from '../../utils/DateFormatter'
import { OwnMessage } from '../../components/message/ownMessage'
import { Message } from '../../components/message/message'
import { IAuthUser } from '../../interfaces/common/auth-user'

const convertMessageArrayToMap = (
  messages: IMessage[],
): Map<string, IMessage[]> => {
  const messagesByDay: Map<string, IMessage[]> = new Map<string, IMessage[]>()

  messages.forEach((message) => {
    const day = getFormattedDateForDivider(message.createdAt)

    if (messagesByDay.has(day)) {
      const messagesBySpecificDay = messagesByDay.get(day)
      if (messagesBySpecificDay) {
        messagesBySpecificDay.push(message)
        messagesByDay.set(day, messagesBySpecificDay)
        return
      }
    }
    messagesByDay.set(day, Array.of(message))
  })

  return messagesByDay
}

const convertMessagesByDayMapToJsx = (
  map: Map<string, IMessage[]>,
  user: IAuthUser | null,
): JSX.Element[] => {
  const renderOutput: JSX.Element[] = []

  map.forEach((messages, day) => {
    renderOutput.push(
      <div key={uuidv4()} className="message-divider">
        {day}
      </div>,
    )
    messages.forEach((message) => {
      if (message.userId === user?.id) {
        renderOutput.push(
          <OwnMessage
            id={message.id}
            key={message.id}
            text={message.text}
            avatar={message.avatar}
            user={message.user}
            editedAt={message.editedAt}
            createdAt={getFormattedTimeForMessage(message.createdAt)}
            liked={message.liked}
          />,
        )
      } else {
        renderOutput.push(
          <Message
            key={message.id}
            id={message.id}
            userId={message.userId}
            avatar={message.avatar}
            user={message.user}
            text={message.text}
            createdAt={getFormattedTimeForMessage(message.createdAt)}
            liked={message.liked}
          />,
        )
      }
    })
  })

  return renderOutput
}

export const prepareMessagesForRender = (
  messages: IMessage[],
  user: IAuthUser | null,
): JSX.Element[] => {
  const messagesByDayMap = convertMessageArrayToMap(messages)

  return convertMessagesByDayMapToJsx(
    messagesByDayMap,
    user,
  )
}
