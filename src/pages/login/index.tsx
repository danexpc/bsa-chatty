import React, { useEffect, useState } from 'react'
import { Button, Form } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { authActionCreator } from '../../store/actions'
import { RootState } from '../../store/store'
import { UserRole } from '../../common/enums/app/user-role.enum'

const LoginFormWrapper = {
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  height: '100%',
  marginTop: '20%',
}

const Login: React.FC = () => {
  const [email, setEmail] = useState<string>('')
  const [password, setPassword] = useState<string>('')

  const dispatch = useDispatch()

  const { user } = useSelector((state: RootState) => state.auth)
  const navigate = useNavigate()

  const handleSubmit = (event: React.FormEvent): void => {
    event.preventDefault()
    dispatch(authActionCreator.loginUser({ email, password }))
  }

  useEffect(() => {
    if (user) {
      if (user.role === UserRole.ADMIN) {
        navigate('/users')
      } else if (user.role === UserRole.USER) {
        navigate('/')
      }
    }
  }, [user])

  return (
    <div style={LoginFormWrapper}>
      <Form onSubmit={handleSubmit}>
        <Form.Group controlId="email">
          <Form.Label>Email</Form.Label>
          <Form.Control
            autoFocus
            type="text"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </Form.Group>
        <Form.Group controlId="password">
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" value={password} onChange={(e) => setPassword(e.target.value)} />
        </Form.Group>
        <Button block size="lg" type="submit" disabled={false}>
          Login
        </Button>
      </Form>
    </div>
  )
}

export default Login
