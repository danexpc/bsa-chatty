export default [
  {
    Header: 'Id',
    accessor: 'id',
    sortType: 'basic',
    sortDirection: 'none',
  },
  {
    Header: 'Name',
    accessor: 'name',
    sortType: 'basic',
    sortDirection: 'none',
  },
  {
    Header: 'Email',
    accessor: 'email',
    sortType: 'basic',
    sortDirection: 'none',
  },
  {
    Header: 'Role',
    accessor: 'role',
    sortType: 'basic',
    sortDirection: 'none',
  },
  {
    Header: 'Action',
    accessor: 'edit',
    sortType: 'basic',
    sortDirection: 'none',
  },
]
