import React, { useEffect } from 'react'
import {
  Button,
  Card,
  CardBody,
  Col,
  Container,
  Row,
} from 'reactstrap'
import { useDispatch, useSelector } from 'react-redux'
import EditIcon from '@material-ui/icons/Edit'
import { useNavigate } from 'react-router-dom'
import ClientSideTable from '../../components/clientSideTable/index.jsx'
import { RootState } from '../../store/store'

import userSchema from './schemas'
import { usersActionCreator } from '../../store/actions'
import { LoadingStatus } from '../../common/enums/app/loading-status.enum'
import { Preloader } from '../../components/preloader'

const Users: React.FC = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const { loading } = useSelector((state: RootState) => state.users)
  const users = useSelector((
    state: RootState,
  ) => state.users.users.map((user) => ({
    ...user,
    edit: <EditIcon onClick={() => navigate(`/users/edit/${user.id}`)} className="edit-icon" />,
  })))

  useEffect(() => {
    dispatch(usersActionCreator.getAllUsers())
  }, [])

  if (loading === LoadingStatus.LOADING) {
    return <Preloader />
  }

  return (
    <div className="mt-5">
      <Container>
        <Row>
          <Col className="col-12">
            <Button color="primary" className="mb-3" onClick={() => navigate('/users/edit')}>Create New User</Button>
            <Card>
              <CardBody>
                <ClientSideTable
                  data={users}
                  columns={userSchema}
                  selectPageSizeLabel="Show entries:"
                  filterPlaceholder="Search"
                />
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </div>
  )
}

export default Users
