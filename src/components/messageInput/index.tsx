import React, { useState } from 'react'
import { Button, Form } from 'react-bootstrap'

import './messageInput.css'
import { useDispatch, useSelector } from 'react-redux'
import { chatActionCreator } from '../../store/actions'
import { RootState } from '../../store/store'

const MessageInput: React.FC = () => {
  const dispatch = useDispatch()

  const { editingMessage, isEditing } = useSelector((state: RootState) => state.chat)

  const [inputText, setLocalText] = useState<string>('')
  const [touched, setTouched] = useState<boolean>(false)
  const [isWarning, setIsWarning] = useState<boolean>(false)

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    if (!touched) {
      setTouched(true)
    }

    setLocalText(e.target.value)
  }

  const handleEditMessage = (): void => {
    if (editingMessage) {
      dispatch(chatActionCreator.updateMessage({
        id: editingMessage.id,
        payload: {
          text: inputText,
          liked: editingMessage.liked,
        },
      }))
    }
  }

  const handleCreateMessage = (): any => dispatch(chatActionCreator.createMessage(
    {
      text: inputText,
      liked: false,
    },
  ))

  const handleSubmit = (): void => {
    if (inputText.trim() === '') {
      setIsWarning(true)
    }

    if (isEditing) {
      handleEditMessage()
    } else {
      handleCreateMessage()
    }

    setLocalText('')
    setTouched(false)
    setIsWarning(false)
  }

  return (
    <div className="message-input">
      <Form.Control
        value={isEditing && !touched ? editingMessage?.text : inputText}
        onChange={handleChange}
        className={`message-input-text ${isWarning ? 'warning' : ''}`}
        type="text"
        placeholder={isWarning ? 'Cannot be empty' : 'Message'}
        size="sm"
      />
      <Button
        onClick={handleSubmit}
        type="submit"
        className="message-input-button"
        size="sm"
      >
        {isEditing ? 'Edit' : 'Send'}
      </Button>
    </div>
  )
}

export default MessageInput
