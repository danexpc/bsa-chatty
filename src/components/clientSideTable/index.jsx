import React from 'react'
import {
  useGlobalFilter,
  usePagination,
  useSortBy,
  useTable,
} from 'react-table'
import Pagination from '@material-ui/lab/Pagination'
import Select from 'react-select'

// @ts-ignore
/* eslint  @typescript-eslint/ban-ts-comment: off */
/* eslint  @typescript-eslint/explicit-function-return-type: off */
/* eslint  react/jsx-props-no-spreading: off */
const ClientSideTable = ({
  data,
  columns,
  selectPageSizeLabel,
  filterPlaceholder,
}) => {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    setGlobalFilter,
    pageCount,
    gotoPage,
    setPageSize,
    state: { pageIndex, pageSize, globalFilter },
  } = useTable(
    {
      columns,
      data,
      initialState: { pageIndex: 0 },
    },
    useGlobalFilter,
    useSortBy,
    usePagination,
  )

  return (
    <>
      <div className="d-flex justify-content-between align-items-center">
        <input
          className="search-label"
          type="text"
          value={globalFilter || ''}
          onChange={(e) => setGlobalFilter(e.target.value)}
          placeholder={filterPlaceholder}
        />
        <div className="d-flex align-items-center gap-2">
          <span style={{ fontWeight: 500 }}>{selectPageSizeLabel}</span>
          <div style={{ width: '80px' }}>
            <Select
              noOptionsMessage={() => 'No options'}
              options={[
                {
                  value: 10,
                  label: '10',
                },
                {
                  value: 20,
                  label: '20',
                },
                {
                  value: 50,
                  label: '50',
                },
              ]}
              onChange={(e) => setPageSize(e.value)}
              value={{
                value: pageSize,
                label: `${pageSize}`,
              }}
            />
          </div>
        </div>
      </div>
      <div>
        <table {...getTableProps()} style={{ width: '100%' }}>
          <thead>
            {headerGroups.map((headerGroup) => (
              <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column) => (
                  <th
                    {...column.getHeaderProps(
                      column.getSortByToggleProps({ title: undefined }),
                    )}
                    {...column.getHeaderProps({
                      style: {
                        minWidth: column.minWidth,
                        width: column.width,
                      },
                    })}
                  >
                    <span className="d-flex w-100">
                      <span className="text-center">
                        {column.render('Header')}
                      </span>
                    </span>
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {page.map((row) => {
              prepareRow(row)
              return (
                <tr {...row.getRowProps()}>
                  {row.cells.map((cell) => (
                    <td
                      {...cell.getCellProps()}
                      className={cell.column.className}
                    >
                      {cell.render('Cell')}
                    </td>
                  ))}
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>

      <div className="d-flex justify-content-center">
        <Pagination
          size="medium"
          siblingCount={1}
          count={pageCount}
          page={pageIndex + 1}
          color="primary"
          shape="rounded"
          onChange={(e, i) => gotoPage(i - 1)}
        />
      </div>
    </>
  )
}

export default ClientSideTable
