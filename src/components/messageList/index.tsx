import React from 'react'

import './messageList.css'

const MessageList: React.FC = ({
  children,
}) => <div className="message-list">{children}</div>

export default MessageList
