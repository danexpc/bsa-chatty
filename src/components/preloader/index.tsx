import React from 'react'
import './preloader.css'

export const Preloader: React.FC = () => (
  <div className="preloader d-flex justify-content-center align-items-center">
    <div className="loading-spinner-wrapper">
      <div className="loading-spinner">
        <div />
        <div />
      </div>
    </div>
  </div>
)
