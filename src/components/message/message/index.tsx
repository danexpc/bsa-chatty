import React from 'react'
import { Card } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faThumbsUp } from '@fortawesome/free-solid-svg-icons'

import '../message.css'
import { useDispatch } from 'react-redux'
import { chatActionCreator } from '../../../store/actions'

interface IMessageProps {
  id: number
  userId: number
  avatar: string
  user: string
  text: string
  createdAt: string
  editedAt?: string
  liked: boolean
}

export const Message: React.FC<IMessageProps> = ({
  id,
  avatar,
  user,
  text,
  createdAt,
  liked,
}) => {
  const dispatch = useDispatch()

  const handleLikeMessage = (): any => {
    dispatch(
      chatActionCreator.updateMessage({
        id,
        payload: {
          text,
          liked: !liked,
        },
      }),
    )
  }

  return (
    <Card className="message">
      <Card.Body className="message-body">
        <div className="message-metadata d-flex align-items-start">
          <Card.Img src={avatar} className="message-user-avatar" />
          <Card.Title className="message-user-name">{user}</Card.Title>
          <Card.Text className="message-time">{createdAt}</Card.Text>
        </div>
        <Card.Text className="message-text">{text}</Card.Text>
        <button
          type="button"
          className={`message-like ${liked ? 'like' : ''}`}
          onClick={handleLikeMessage}
        >
          <FontAwesomeIcon icon={faThumbsUp} />
        </button>
      </Card.Body>
    </Card>
  )
}
