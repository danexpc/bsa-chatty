import React from 'react'
import { Card } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEdit, faTrash } from '@fortawesome/free-solid-svg-icons'

import '../message.css'
import { useDispatch } from 'react-redux'
import { chatActionCreator } from '../../../store/actions'

interface IOwnMessageProps {
  id: number
  text: string
  avatar: string
  user: string
  editedAt: Date | undefined
  createdAt: string
  liked: boolean
}

export const OwnMessage: React.FC<IOwnMessageProps> = ({
  id,
  text,
  avatar,
  user,
  createdAt,
  editedAt,
  liked,
}) => {
  const dispatch = useDispatch()

  const handleEditMessage = ()
    : any => dispatch(
    chatActionCreator.setMessage({
      id,
      text,
      liked,
    }),
  )

  const handleDeleteMessage = (): any => dispatch(chatActionCreator.deleteMessage(id))

  return (
    <Card className="own-message">
      <Card.Body className="own-message-body">
        <div className="message-metadata d-flex align-items-start">
          <Card.Img src={avatar} className="message-user-avatar" />
          <Card.Title className="message-user-name">{user}</Card.Title>
          <Card.Text className="message-time">
            {(editedAt ? '(edited) ' : '') + createdAt}
          </Card.Text>
        </div>
        <Card.Text className="message-text">{text}</Card.Text>
        <button
          type="button"
          className="message-edit"
          onClick={handleEditMessage}
        >
          <FontAwesomeIcon icon={faEdit} />
        </button>
        <button
          type="button"
          className="message-delete"
          onClick={handleDeleteMessage}
        >
          <FontAwesomeIcon icon={faTrash} />
        </button>
      </Card.Body>
    </Card>
  )
}
