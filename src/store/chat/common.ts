const ActionType = {
  SET_MESSAGE: 'chat/set-message',
  GET_ALL_MESSAGES: 'chat/get-all-messages',
  UPDATE_MESSAGE: 'chat/update-message',
  CREATE_MESSAGE: 'chat/create-message',
  DELETE_MESSAGE: 'chat/delete-message',
}

export { ActionType }
