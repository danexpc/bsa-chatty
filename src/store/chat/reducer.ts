import { createReducer, isAnyOf } from '@reduxjs/toolkit'
import { LoadingStatus } from '../../common/enums/app/loading-status.enum'
import {
  createMessage,
  deleteMessage,
  getAllMessages,
  setMessage,
  updateMessage,
} from './actions'
import { IMessage } from '../../interfaces/common/message'
import { IMessageSet } from '../../interfaces/common/set-message'

interface IInitialState {
  messages: IMessage[]
  loading: string,
  editingMessage: IMessageSet | null,
  isEditing: boolean
}

const initialState: IInitialState = {
  messages: [],
  loading: LoadingStatus.IDLE,
  editingMessage: null,
  isEditing: false,
}

/* eslint no-param-reassign: off */
const reducer = createReducer(initialState, (builder) => {
  builder.addCase(setMessage, (state, action) => {
    state.editingMessage = action.payload
  })
  builder.addCase(getAllMessages.fulfilled, (state, action) => {
    state.messages = action.payload
    state.loading = LoadingStatus.SUCCEEDED
  })
  builder.addCase(createMessage.fulfilled, (state, action) => {
    state.messages.push(action.payload)
    state.loading = LoadingStatus.SUCCEEDED
  })
  builder.addCase(updateMessage.fulfilled, (state, action) => {
    const index = state.messages.findIndex((message) => message.id === action.payload.id)
    state.messages[index].text = action.payload.text
    state.messages[index].liked = action.payload.liked
    state.loading = LoadingStatus.SUCCEEDED
  })
  builder.addCase(deleteMessage.fulfilled, (state, action) => {
    state.messages = state.messages.filter((message) => message.id !== action.payload)
    state.loading = LoadingStatus.SUCCEEDED
  })
  builder.addMatcher(isAnyOf(getAllMessages.pending, createMessage.pending,
    updateMessage.pending, deleteMessage.pending), (state, action) => {
    state.loading = LoadingStatus.LOADING
  })
  builder.addMatcher(isAnyOf(getAllMessages.rejected, createMessage.rejected,
    updateMessage.rejected, deleteMessage.rejected), (state, action) => {
    state.loading = LoadingStatus.FAILED
  })
})

export { reducer }
