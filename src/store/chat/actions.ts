import { createAction, createAsyncThunk } from '@reduxjs/toolkit'
import { AppDispatch } from '../store'
import { IMessage } from '../../interfaces/common/message'
import { ActionType } from './common'
import { IMessageUpdate } from '../../interfaces/request/message-update'
import { IMessageSet } from '../../interfaces/common/set-message'

const setMessage = createAction(ActionType.SET_MESSAGE, (payload: IMessageSet) => ({
  payload,
}))

const getAllMessages = createAsyncThunk<
  IMessage[],
  void,
  {
    dispatch: AppDispatch
    extra: {
      services: any
    }
  }
>(ActionType.GET_ALL_MESSAGES, async (_, { extra: { services } }) => {
  const { data } = await services.message.getAllMessages()
  return data
})

const createMessage = createAsyncThunk<
  IMessage,
  IMessageUpdate,
  {
    dispatch: AppDispatch
      extra: {
        services: any
      }
  }
  >(ActionType.CREATE_MESSAGE, async (payload, { extra: { services } }) => {
    const { data } = await services.message.createMessage(payload)
    return data
  })

const updateMessage = createAsyncThunk<
    IMessage,
    {
        id: number,
        payload: IMessageUpdate
    },
    {
        dispatch: AppDispatch
        extra: {
            services: any
        }
    }
    >(ActionType.UPDATE_MESSAGE, async ({ id, payload }, { extra: { services } }) => {
      const { data } = await services.message.updateMessage(id, payload)
      return data
    })

const deleteMessage = createAsyncThunk<
    number,
    number,
    {
        dispatch: AppDispatch
        extra: {
            services: any
        }
    }
      >(ActionType.DELETE_MESSAGE, async (id, { extra: { services } }) => {
        await services.message.deleteMessage(id)
        return id
      })

export {
  getAllMessages,
  createMessage,
  updateMessage,
  deleteMessage,
  setMessage,
}
