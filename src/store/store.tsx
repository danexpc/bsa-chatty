import { combineReducers, configureStore } from '@reduxjs/toolkit'
import { persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import { authReducer, chatReducer, usersReducer } from './root-reducer'
import * as services from '../services/services'

const rootReducer = combineReducers({
  auth: authReducer,
  chat: chatReducer,
  users: usersReducer,
})

const persistConfig = {
  key: 'root',
  debug: true,
  storage,
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

export const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({
    thunk: {
      extraArgument: { services },
    },
    serializableCheck: false,
  }),
})

export type RootState = ReturnType<typeof store.getState>

export type AppDispatch = typeof store.dispatch
