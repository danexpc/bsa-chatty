import { createReducer } from '@reduxjs/toolkit'
import { LoadingStatus } from '../../common/enums/app/loading-status.enum'
import { loginUser } from './actions'
import { IAuthUser } from '../../interfaces/common/auth-user'

interface IInitialState {
  user: IAuthUser | null,
  loading: string
}

/* eslint no-param-reassign: off */
const initialState: IInitialState = {
  user: null,
  loading: LoadingStatus.IDLE,
}

const reducer = createReducer(initialState, (builder) => {
  builder.addCase(loginUser.pending, (state) => {
    state.loading = LoadingStatus.LOADING
  })
  builder.addCase(loginUser.fulfilled, (state, action) => {
    state.user = action.payload
    state.loading = LoadingStatus.SUCCEEDED
  })
  builder.addCase(loginUser.rejected, (state) => {
    state.loading = LoadingStatus.FAILED
  })
})

export { reducer }
