import { createAsyncThunk } from '@reduxjs/toolkit'
import { ActionType } from './common'
import { AppDispatch } from '../store'
import { IAuthUser } from '../../interfaces/common/auth-user'
import { IUserLogin } from '../../interfaces/request/user-login'
import { StorageKey } from '../../common/enums/app/storage-key.enum'

const loginUser = createAsyncThunk<
  IAuthUser,
  IUserLogin,
  {
    dispatch: AppDispatch
    extra: {
      services: any
    }
  }
>(ActionType.LOG_IN, async (request, { extra: { services } }) => {
  const { data } = await services.auth.login(request)
  services.storage.setItem(StorageKey.TOKEN, data.token)

  return data
})

export { loginUser }
