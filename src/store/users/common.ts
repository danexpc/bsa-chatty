const ActionType = {
  GET_ALL_USERS: 'users/get-all-users',
  GET_USER: 'users/get-user',
  CREATE_USER: 'users/create-user',
  UPDATE_USER: 'users/update-user',
  DELETE_USER: 'users/delete-user',
}

export { ActionType }
