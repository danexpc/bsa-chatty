import { createAsyncThunk } from '@reduxjs/toolkit'
import { AppDispatch } from '../store'
import { ActionType } from './common'
import { IUser } from '../../interfaces/common/user'
import { IUserRequest } from '../../interfaces/request/user-request'

const getAllUsers = createAsyncThunk<
  IUser[],
  void,
  {
    dispatch: AppDispatch
    extra: {
      services: any
    }
  }
>(ActionType.GET_ALL_USERS, async (_, { extra: { services } }) => {
  const { data } = await services.user.getAllUsers()
  return data
})

const getUser = createAsyncThunk<
    IUser,
    number,
    {
        dispatch: AppDispatch
        extra: {
            services: any
        }
    }
    >(ActionType.GET_USER, async (id, { extra: { services } }) => {
      const { data } = await services.user.getUser(id)
      return data
    })

const createUser = createAsyncThunk<
    IUser,
    IUserRequest,
    {
        dispatch: AppDispatch
        extra: {
            services: any
        }
    }
    >(ActionType.CREATE_USER, async (payload, { extra: { services } }) => {
      const { data } = await services.user.createUser(payload)
      return data
    })

const updateUser = createAsyncThunk<
    IUser,
    {
        id: number,
        payload: IUserRequest
    },
    {
        dispatch: AppDispatch
        extra: {
            services: any
        }
    }
    >(ActionType.UPDATE_USER, async ({ id, payload }, { extra: { services } }) => {
      const { data } = await services.user.updateUser(id, payload)
      return data
    })

const deleteUser = createAsyncThunk<
    number,
    number,
    {
        dispatch: AppDispatch
        extra: {
            services: any
        }
    }
    >(ActionType.DELETE_USER, async (id, { extra: { services } }) => {
      await services.user.deleteUser(id)
      return id
    })

export {
  getAllUsers,
  getUser,
  createUser,
  updateUser,
  deleteUser,
}
