import { createReducer, isAnyOf } from '@reduxjs/toolkit'
import { LoadingStatus } from '../../common/enums/app/loading-status.enum'
import {
  createUser,
  deleteUser,
  getAllUsers,
  getUser,
  updateUser,
} from './actions'
import { IUser } from '../../interfaces/common/user'

interface IInitialState {
  users: IUser[]
  loading: string
  user: IUser | null
}

const initialState: IInitialState = {
  users: [],
  loading: LoadingStatus.IDLE,
  user: null,
}

/* eslint no-param-reassign: off */
const reducer = createReducer(initialState, (builder) => {
  builder.addCase(getAllUsers.fulfilled, (state, action) => {
    state.users = action.payload
    state.loading = LoadingStatus.SUCCEEDED
  })
  builder.addCase(getUser.fulfilled, (state, action) => {
    state.user = action.payload
    state.loading = LoadingStatus.SUCCEEDED
  })
  builder.addCase(createUser.fulfilled, (state, action) => {
    state.users.push(action.payload)
    state.loading = LoadingStatus.SUCCEEDED
  })
  builder.addCase(updateUser.fulfilled, (state, action) => {
    const index = state.users.findIndex((userI) => userI.id === action.payload.id)
    state.users[index].name = action.payload.name
    state.users[index].email = action.payload.email
    state.users[index].role = action.payload.role
    state.users[index].avatar = action.payload.avatar
    state.loading = LoadingStatus.SUCCEEDED
  })
  builder.addCase(deleteUser.fulfilled, (state, action) => {
    state.users = state.users.filter((userI) => userI.id !== action.payload)
    state.loading = LoadingStatus.SUCCEEDED
  })
  builder.addMatcher(isAnyOf(getUser.pending, getAllUsers.pending, createUser.pending,
    updateUser.pending, deleteUser.pending), (state, action) => {
    state.loading = LoadingStatus.LOADING
  })
  builder.addMatcher(isAnyOf(getUser.rejected, getAllUsers.rejected, createUser.rejected,
    updateUser.rejected, deleteUser.rejected), (state, action) => {
    state.loading = LoadingStatus.FAILED
  })
})

export { reducer }
