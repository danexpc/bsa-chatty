import auth from './auth/auth.service'
import message from './message/message.service'
import user from './user/user.service'
import storage from './storage/storage.service'

export {
  auth,
  message,
  user,
  storage,
}
