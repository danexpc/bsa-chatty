import {
  del,
  get,
  post,
  put,
} from '../../helpers/http.helper'
import { IMessageUpdate } from '../../interfaces/request/message-update'

const Message = (() => {
  const getAllMessages = (): Promise<any> => get('/api/Messages')
  const createMessage = (payload: IMessageUpdate): Promise<any> => post('/api/Messages', payload)
  const updateMessage = (id: number, payload: IMessageUpdate): Promise<any> => put(`/api/Messages/${id}`, payload)
  const deleteMessage = (id: number): Promise<any> => del(`/api/Messages/${id}`)

  return {
    getAllMessages,
    createMessage,
    updateMessage,
    deleteMessage,
  }
})()

export default Message
