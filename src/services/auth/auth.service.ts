import { post } from '../../helpers/http.helper'
import { IUserLogin } from '../../interfaces/request/user-login'

const Auth = (() => {
  const login = (request: IUserLogin)
      : Promise<any> => post('/api/Auth/login', request)

  return {
    login,
  }
})()

export default Auth
