import { IMessage } from '../../interfaces/common/message'

async function fetchRequest(request: Request): Promise<IMessage[]> {
  const response = await fetch(request)
  return response.json()
}

export class ApiService {
  url = ''

  constructor(url: string) {
    this.url = url
  }

  async fetchMessages(): Promise<IMessage[]> {
    try {
      const request = new Request(`${this.url}`, {
        method: 'get',
      })
      return fetchRequest(request)
    } catch (error) {
      console.error(error)
    }
    return []
  }
}
