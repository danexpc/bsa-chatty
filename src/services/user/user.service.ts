import {
  del,
  get,
  post,
  put,
} from '../../helpers/http.helper'
import { IUserRequest } from '../../interfaces/request/user-request'

const User = (() => {
  const getAllUsers = (): Promise<any> => get('/api/Users')
  const getUser = (id: number): Promise<any> => get(`/api/Users/${id}`)
  const createUser = (payload: IUserRequest): Promise<any> => post('/api/Users', payload)
  const updateUser = (id: number, payload: IUserRequest): Promise<any> => put(`/api/Users/${id}`, payload)
  const deleteUser = (id: number): Promise<any> => del(`/api/Users/${id}`)
  return {
    getAllUsers,
    getUser,
    createUser,
    updateUser,
    deleteUser,
  }
})()

export default User
