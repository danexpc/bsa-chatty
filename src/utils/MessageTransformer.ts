import { IMessage } from '../interfaces/common/message'

const MessageTransformer = (() => {
  const transform = (message: IMessage): IMessage => ({
    id: message.id,
    userId: message.userId,
    avatar: message.avatar,
    user: message.user,
    text: message.text,
    createdAt: new Date(message.createdAt),
    editedAt: message.editedAt ? new Date(message.editedAt) : undefined,
    liked: message.liked,
  })

  const transformAll = (
    messages: IMessage[],
  ): IMessage[] => messages.map((message) => transform(message))

  return {
    transform,
    transformAll,
  }
})()

export default MessageTransformer
