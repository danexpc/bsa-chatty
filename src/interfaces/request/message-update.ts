export interface IMessageUpdate {
    text: string,
    liked: boolean,
}
