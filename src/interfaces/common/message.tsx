export interface IMessage {
  id: number
  userId: number
  avatar: string
  user: string
  text: string
  createdAt: Date
  editedAt?: Date
  liked: boolean
}
