export interface IMessageSet {
    id: number
    text: string,
    liked: boolean
}
