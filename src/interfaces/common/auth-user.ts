export interface IAuthUser {
    id: number,
    email: string
    name: string,
    role: string,
    token: string
}
