export interface IUser {
  id: null
  name: string
  email: string
  role: string
  avatar: string
}
