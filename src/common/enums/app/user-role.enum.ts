const UserRole = {
  USER: 'User',
  ADMIN: 'Admin',
}

export { UserRole }
