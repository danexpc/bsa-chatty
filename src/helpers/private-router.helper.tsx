import { useSelector } from 'react-redux'
import React from 'react'
import { Navigate } from 'react-router-dom'
import { RootState } from '../store/store'

interface Props {
  component: React.ComponentType
  path?: string
  roles: Array<string>
}

export const PrivateRoute: React.FC<Props> = ({
  component: RouteComponent,
  roles,
}) => {
  const { user } = useSelector((state: RootState) => state.auth)
  const isAuthenticated = !!user
  const userHasRequiredRole = !!(user && roles.includes(user.role))

  if (!isAuthenticated) {
    return <Navigate to="/login" />
  }

  if (isAuthenticated && userHasRequiredRole) {
    return <RouteComponent />
  }

  return <Navigate to="/" />
}
