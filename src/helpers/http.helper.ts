import axios, { AxiosRequestConfig } from 'axios'
import { ENV } from '../common/enums/app/env.enum'
import { storage } from '../services/services'
import { StorageKey } from '../common/enums/app/storage-key.enum'

const axiosApi = axios.create({
  baseURL: ENV.API_PATH,
})

/* eslint no-param-reassign: "error" */
export default {
  setupInterceptors: (store: any) => {
    axiosApi.interceptors.request.use(
      (config: AxiosRequestConfig) => {
        if (
          config.url !== '/api/Auth/login'
            && config.url !== '/api/Auth/register'
        ) {
          const token = storage.getItem(StorageKey.TOKEN)

          if (config.headers) {
            config.headers.Authorization = `Bearer ${token}`
          }
        }
        return config
      },
      (error) => Promise.reject(error),
    )

    axiosApi.interceptors.response.use(
      (response) => response,
      (error) => {
        if (error.response.status === 401) {
          // store.dispatch(userLogout());
        }
        return Promise.reject(error)
      },
    )
  },
}

const get = (url: string, config = {})
    : Promise<any> => axiosApi.get(url, { ...config })

const post = (url: string, data: any, config = {})
    : Promise<any> => axiosApi.post(url, { ...data }, { ...config })

const put = (
  url: string,
  data: any,
  config = {},
): Promise<any> => axiosApi.put(url, { ...data }, { ...config })

const patch = (
  url: string,
  data: Record<string, unknown>,
  config = {},
): Promise<any> => axiosApi.patch(url, { ...data }, { ...config })

const del = (
  url: string, config = {},
): Promise<any> => axiosApi.delete(url, { ...config })

export {
  get,
  post,
  put,
  patch,
  del,
}
